#!/usr/bin/python3
import sys
from collections import defaultdict

qseqDict = defaultdict(list)
for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	qseqid = ssline[0]
	sseqid = ssline[1]
	start = ssline[8]
	stop = ssline[9]
	key = qseqid+'|'+sseqid
	qseqDict[key].append([int(start)-1000,int(stop)+1000])

groupDict = defaultdict(dict)
for key,values in qseqDict.items():
	genesAreas = []
	for val in values:
		start,stop = val
		start = int(start)
		stop = int(stop)
		bool = False
		for i in range(0,len(genesAreas)):
			areaStart,areaStop = genesAreas[i]
			if start < areaStart < stop:
				genesAreas[i][0] = start
				if stop > areaStop:
					genesAreas[i][1] = stop
				bool = True
			elif start < areaStop < stop:
				genesAreas[i][1] = stop
				bool = True
			elif areaStart <= start <= areaStop:
				bool = True
			elif areaStart <= stop <= areaStop:
				bool = True
		if bool == False:
			genesAreas.append([start,stop])
	count = 0
	for area in genesAreas:
		start,stop = area
		groupDict[key].setdefault(count,[])
		groupDict[key][count] = [start,stop]
		count += 1

for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	qseqid = ssline[0]
	sseqid = ssline[1]
	start = int(ssline[8])
	stop = int(ssline[9])
	if start > 1001:
		start -= 1000
	else:
		start = 1
	stop += 1000
	group = ''
	for groupNumber in groupDict[qseqid+'|'+sseqid]:
		groupStart,groupStop = groupDict[qseqid+'|'+sseqid][groupNumber]
		if groupStart <= start <= groupStop or groupStart <= stop <= groupStop:
			group = 'group='+qseqid+'|'+sseqid+'|'+str(groupNumber)+'; '
	print(sseqid+'\tMaarten\texonpart\t'+str(start)+'\t'+str(stop)+'\t.\t+\t.\t'+group+'source=M')
